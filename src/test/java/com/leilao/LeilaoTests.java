package com.leilao;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.management.InvalidAttributeValueException;

class LeilaoTests {

	@Test
	public void testeValidarLanceOK() {
		Usuario usuario = new Usuario(1, "Cynthia");
		Lance lance = new Lance(usuario, 100.00);
		Leilao leilao = new Leilao();
		List<Lance> lances = new ArrayList<>();
		leilao.setLances(lances);

		Assertions.assertTrue(leilao.validarLance(lance));
	}


	@Test
	public void testeValidarLanceNOK() {
		Usuario usuario = new Usuario(2, "Fabio");
		Lance lance = new Lance(usuario, 0.00);
		Leilao leilao = new Leilao();
		List<Lance> lances = new ArrayList<>();
		leilao.setLances(lances);

		Assertions.assertFalse(leilao.validarLance(lance));
	}

	@Test
	public void testarValidacaoDeLanceMenorLance(){
		Usuario usuario = new Usuario(1, "Vinicius");
		Lance lance = new Lance(usuario, 100.00);
		Leilao leilao = new Leilao();
		List<Lance> lances = new ArrayList(Arrays.asList(new Lance(usuario, 1000.00)));
		leilao.setLances(lances);

		Assertions.assertFalse(leilao.validarLance(lance));
	}


	@Test
	public void testeAdicionarNovoLance1() throws InvalidAttributeValueException {
		Usuario usuario = new Usuario(1, "Cynthia");
		Lance lance = new Lance(usuario, 100.00);

		Leilao leilao = new Leilao(new ArrayList());

		Assertions.assertEquals(leilao.adicionarLance(lance), lance);
	}

	@Test
	public void testeAdicionarLanceInvalido()  {
		Usuario usuario = new Usuario(1, "Debora");
		Lance lance = new Lance(usuario, 0.0);

		Leilao leilao = new Leilao(new ArrayList());

		Assertions.assertThrows(InvalidAttributeValueException.class, () -> {leilao.adicionarLance(lance);});
	}
}
