package com.leilao;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class LeiloeiroTests {

    @Test
    public void testeRetornarMaiorLance() {
        List<Lance> lances = new ArrayList<>();

        Usuario usuario = new Usuario(1, "Cynthia");
        Lance maiorLance = new Lance(usuario, 1000.00);
        lances.add(maiorLance);

        Usuario usuario2 = new Usuario(2, "Fabio");
        lances.add(new  Lance (usuario2, 50.00));

        Usuario usuario3 = new Usuario(3, "Debora");
        lances.add(new Lance (usuario3, 200.00));

        Leilao leilao = new Leilao(lances);
        Leiloeiro leiloeiro = new Leiloeiro("Cezar", leilao);

        Assertions.assertEquals(1000.00, leiloeiro.retornarMaiorLance().getValorDoLance());
        Assertions.assertEquals("Cynthia", leiloeiro.retornarMaiorLance().getUsuario().getNome());
        Assertions.assertEquals(leiloeiro.retornarMaiorLance(), maiorLance);
    }

}
